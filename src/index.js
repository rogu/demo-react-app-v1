import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from "react-redux";
import thunk from 'redux-thunk';
import { itemsReducer, registerReducer, itemDetailReducer, authReducer, workersReducer, cartReducer } from "./containers";
import { setCORSInterceptors } from './utils/cors-interceptor';

setCORSInterceptors();

const reducers = combineReducers({
    items: itemsReducer,
    itemDetail: itemDetailReducer,
    register: registerReducer,
    auth: authReducer,
    workers: workersReducer,
    cart: cartReducer
});

const store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
