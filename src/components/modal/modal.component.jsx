import React, { Component } from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

Modal.setAppElement(document.getElementById('root'));

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0, 0.6)'
    }
};

export default class ModalComponent extends Component {

    static propTypes = {
        text: PropTypes.string.isRequired,
        active: PropTypes.bool
    }

    static defaultProps = {
        active: true
    };

    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
    }

    render() {
        return <div>
            <button className={'btn btn-dark'} disabled={!this.props.active} onClick={this.openModal}>{this.props.text}</button>
            <Modal
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.closeModal}
                style={customStyles}>
                <div>
                    <h5 className={'d-inline'}>{this.props.text}</h5>
                    <button className={'float-right btn btn-sm'} onClick={this.closeModal}>&times;</button>
                </div>
                <hr />
                {this.props.children}
            </Modal>
        </div>
    }
}
