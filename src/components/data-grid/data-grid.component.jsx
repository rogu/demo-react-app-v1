import React, { PureComponent } from "react";
import PropTypes from "prop-types";

export const FieldTypes = {
  input: 'input',
  image: 'image'
}

class DataGridComponent extends PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired,
    config: PropTypes.array.isRequired,
    logged: PropTypes.bool,
    remove: PropTypes.func,
    update: PropTypes.func,
    more: PropTypes.func
  };

  action = (evt) => {
    const id = evt.currentTarget.dataset.id;
    const action = evt.target.dataset.action;
    const fn = this.props[action];
    fn && fn(id);
  }

  render() {
    return (
      <div>
        <table className="table table-bordered">
          <thead>
            <tr>
              {this.props.config.map((head, idx) => {
                return <th key={idx}>{(head.header || head.key).toUpperCase()}</th>;
              })}
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((item, idx) => {
              return (
                <tr key={item.id}>
                  {this.props.config.map((field, idx) => {
                    switch (field.type) {
                      case FieldTypes.input:
                        return (
                          <td key={idx}>
                            <input
                              type="text"
                              name={field.key}
                              data-id={item.id}
                              onChange={this.props.update}
                              className={"form-control"}
                              disabled={!this.props.logged}
                              defaultValue={item[field.key]}
                            />
                          </td>
                        );
                      case FieldTypes.image:
                        return (
                          <td key={idx}>
                            <img src={item[field.key]} width="50" alt="img" />
                          </td>
                        );
                      default:
                        return <td key={idx}>{item[field.key]}</td>;
                    }
                  })}
                  <td onClick={this.action} data-id={item.id}>
                    {this.props.children}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default DataGridComponent;
