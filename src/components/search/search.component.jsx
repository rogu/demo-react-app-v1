import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SearchComponent extends Component {

    static propTypes = {
        controls: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    searchBy = ({ target: { dataset: { key }, value } }) => {
        this.setState({ [key]: value }, () => {
            this.props.onChange({ ...this.state });
        });
    }

    render() {
        return (
            <div className={'card card-body'}>
                {this.props.controls.map((control, idx) => {
                    return <label key={idx}>
                        by {control}
                        <br />
                        {<input type="text"
                            data-key={control}
                            onChange={this.searchBy}
                            className={'form-control'} />}
                    </label>
                })}
            </div>
        );
    }
}
