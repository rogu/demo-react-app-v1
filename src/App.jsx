import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch, NavLink, Redirect } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { ItemDetailsContainer, ItemsContainer, RegisterContainer, WorkersContainer, AuthContainer, CartContainer } from './containers';
import { LinkComponent, NotFoundComponent } from './components';
import "reusable-webc/dist/footer.js";

class App extends Component {
  render() {
    return <div className="App">
      <Router>
        <div>
          <nav className="navbar navbar-expand-sm bg-light border-bottom mb-3">
            <div className="nav-item">
              <NavLink className="nav-link" activeClassName="nav-active" to="/items">
                items
                </NavLink>
            </div>
            <div className="nav-item">
              <NavLink className="nav-link" activeClassName="nav-active" to="/register">
                register
                </NavLink>
            </div>
            <div className="nav-item">
              <NavLink className="nav-link" activeClassName="nav-active" to="/workers">
                workers
                </NavLink>
            </div>
            <div className="ml-2">
              <AuthContainer />
            </div>
            <div className="ml-2">
              <CartContainer />
            </div>
          </nav>
          <div className="container-fluid">
            <Switch>
              <Route exact path="/"><Redirect to="items" /></Route>
              <Route exact path="/items" component={ItemsContainer} />
              <Route path="/items/:id" component={ItemDetailsContainer} />
              <Route path="/register" component={RegisterContainer} />
              <Route path="/workers" component={WorkersContainer} />
              <Route component={NotFoundComponent} />
            </Switch>
          </div>
        </div>
      </Router>
      <h1>Szkolenie React - demo</h1>
      <div>
        <course-footer courses></course-footer>
        <LinkComponent page="https://debugger.pl">wszystkie szkolenia</LinkComponent>
      </div>
    </div>;
  }
}

export default App;
