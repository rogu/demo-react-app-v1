export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const GET_ITEM_SUCCESS = "GET_ITEM_SUCCESS";
export const USER_LOGGED = 'USER_LOGGED';
export const FETCH_WORKERS_SUCCESS = 'FETCH_WORKERS_SUCCESS';
export const SEARCH_WORKERS = 'SEARCH_WORKERS';

export function Action(type, payload) {
    return { type, payload };
}
