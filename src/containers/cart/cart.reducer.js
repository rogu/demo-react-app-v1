import { ADD_TO_CART, REMOVE_FROM_CART, GET_CART_SUCCESS } from "./cart.actions";

export const cartReducer = (state = { data: [] }, action) => {
    let exists = state.data.find((item) => item.id === action.payload.id);
    switch (action.type) {
        case GET_CART_SUCCESS:
            return { ...state, ...action.payload };
        case ADD_TO_CART:
            exists ? exists.count++ : state.data.push({ ...action.payload, count: 1 });
            return { ...state, data: [...state.data] };
        case REMOVE_FROM_CART:
            exists.count === 1 ? state.data = state.data.filter((item) => item !== exists) : exists.count--;
            return { ...state, data: [...state.data] };
        default:
            return state;
    }
}
