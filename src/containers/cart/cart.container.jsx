import React, { Component } from 'react';
import { connect } from "react-redux";
import { ModalComponent, DataGridComponent } from '../../components';
import * as actions from './cart.actions';

class CartContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.getCart();
    }

    render() {
        return (<div>
            <ModalComponent text={'total cart items ' + this.props.count}>
                {this.props.count
                    ? <DataGridComponent
                        logged={true}
                        remove={this.props.remove}
                        add={this.props.add}
                        data={this.props.data}
                        config={[{ key: 'title' }, { key: 'count' }]}>
                        <button data-action='remove'>-</button>
                        <button data-action='add'>+</button>
                    </DataGridComponent>
                    : 'there is nothing to show'
                }
            </ModalComponent>
        </div>);
    }
}

const mapStateToProps = (state) => {

    return { ...state.cart, count: state.cart.data.reduce((acc, item) => acc + item.count, 0) };
}

const mapDispatchToProps = dispatch => {
    return {
        getCart() {
            dispatch(actions.getCart());
        },
        remove(id) {
            dispatch(actions.removeItem(id));
        },
        add(id) {
            dispatch(actions.addItem(id));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CartContainer);
