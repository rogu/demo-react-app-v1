import { Action } from "../actions";

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const LOCALSTORAGE_KEY = 'REACT_APP_CART';
export const GET_CART_SUCCESS = 'GET_CART_SUCCESS';

export function updateLocalStorage(cart) {
    localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(cart));
}

export function getCart() {
    return (dispatch) => {
        const cart = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
        dispatch(new Action(GET_CART_SUCCESS, cart));
    }
}

export function addItem(id) {
    return (dispatch, getState) => {
        const state = getState();
        const item = state.items.data.find(item => item.id === id);
        dispatch(new Action(ADD_TO_CART, item));
        updateLocalStorage(state.cart);
    }
}

export function removeItem(id) {
    return (dispatch, getState) => {
        const state = getState();
        dispatch(new Action(REMOVE_FROM_CART, { id }));
        updateLocalStorage(state.cart);
    }
}
