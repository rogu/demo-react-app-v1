import React, { Component } from 'react';
import { connect } from "react-redux";
import * as actions from './auth.actions';
import { ModalComponent } from "../../components";

class AuthContainer extends Component {
    constructor(props) {
        super(props);
        this.modal = React.createRef();
    }

    componentDidMount() {
        this.props.isLogged();
    }

    sendForm = (evt) => {
        evt.preventDefault();
        const { username, password } = evt.target;
        this.props.logIn(username.value, password.value);
        this.modal.current.closeModal();
    }

    render() {
        return <div>
            {this.props.logged
                ? <button onClick={this.props.logOut} className={'btn btn-warning'}>log out</button>
                : <ModalComponent ref={this.modal} text={'log in'}>
                    <form className="form-inline" onSubmit={this.sendForm}>
                        <input className="form-control mr-2"
                            type="email"
                            name="username"
                            defaultValue={'admin@localhost'}
                            placeholder="username" />
                        <input className="form-control mr-2"
                            type="password"
                            name="password"
                            defaultValue={'admin'}
                            placeholder="password" />
                        <button className="btn btn-primary">
                            send
                        </button>
                    </form>
                </ModalComponent>}
        </div>
    }
}

const mapStateToProps = (state) => ({ ...state.auth });
const mapDispatchToProps = (dispatch) => {
    return {
        logIn(username, password) {
            dispatch(actions.logIn(username, password));
        },
        isLogged() {
            dispatch(actions.isLogged());
        },
        logOut() {
            dispatch(actions.logOut());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer);
