import { USER_LOGGED } from "../actions";

export function authReducer(state = { logged: false }, action) {
    switch (action.type) {
        case USER_LOGGED:
            return { ...state, logged: action.payload };
        default:
            return state;
    }
}
