import { USER_LOGGED, Action } from "../actions";
import { Api } from "../../utils/api";
import axios from 'axios';
const STORAGE_KEY = 'token';

export function logIn(username, password) {
    return dispatch => {
        axios
            .post(Api.LOGIN_END_POINT, { username, password })
            .then((resp) => {
                localStorage.setItem(STORAGE_KEY, resp.data.accessToken);
                dispatch(new Action(USER_LOGGED, true));
            });
    }
}

export function isLogged() {
    return dispatch => {
        axios
            .get(Api.LOGGED_END_POINT)
            .then((resp) => dispatch(new Action(USER_LOGGED, resp.data.error ? false : true)));
    }
}

export function logOut() {
    return dispatch => {
        localStorage.removeItem(STORAGE_KEY);
        dispatch(new Action(USER_LOGGED, false));
    }
}
