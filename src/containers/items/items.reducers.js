import { FETCH_ITEMS_SUCCESS } from "../actions";
import { FieldTypes } from "../../components/data-grid/data-grid.component";

const initialState = {
  data: [],
  dgConfig: [
    { key: "title" },
    { key: "price", type: FieldTypes.input },
    { key: "imgSrc", type: "image", header: FieldTypes.image }
  ]
};

export function itemsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_ITEMS_SUCCESS:
      return { ...state, data: payload.data, total: payload.total };
    default:
      return state;
  }
}
