import axios from 'axios';
import { FETCH_ITEMS_SUCCESS, Action } from "../actions";
import { ADD_TO_CART, updateLocalStorage } from "../cart/cart.actions";
import { Api } from "../../utils/api";
import qs from 'qs';

export function fetchItems(query) {
    return dispatch => {
        axios
            .get(`${Api.ITEMS_END_POINT}?${qs.stringify(query)}`)
            .then((resp) => {
                dispatch(new Action(FETCH_ITEMS_SUCCESS, resp.data));
            });
    }
}

export const addToCard = (id) => {
    return (dispatch, getState) => {
        const { items, card } = getState();
        const item = items.data.find(item => item.id === id);
        dispatch(new Action(ADD_TO_CART, item));
        updateLocalStorage(card);
    }
}
