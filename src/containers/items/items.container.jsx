import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./items.actions";
import { SearchComponent, DataGridComponent } from "../../components"
import Pagination from "react-js-pagination";
import { BehaviorSubject } from "rxjs";
import { debounceTime } from "rxjs/operators";

class ItemsContainer extends Component {
  constructor(props) {
    super(props);
    this.modal = React.createRef();

    this.filters = new BehaviorSubject({
      itemsPerPage: 5,
      currentPage: 1
    });

    this.more = this.props.more.bind(this);
  }

  componentDidMount() {
    this.filters
      .pipe(debounceTime(500))
      .subscribe((value) => this.props.fetch.call(this));
  }

  handlePageChange = page => {
    this.filters.next({
      ...this.filters.getValue(),
      currentPage: page
    })
  };

  search = state => {
    this.filters.next({
      ...this.filters.getValue(),
      ...state,
      currentPage: 1
    })
  };

  render() {
    return (
      <div className={"row"}>
        <div className="col-3">
          <SearchComponent
            onChange={this.search}
            controls={["title", "priceFrom"]}
          />
        </div>
        <div className="col-9">
          <div className="form-row mb-2">
            <div className="col-2">
              <select
                className={"form-control"}
                value={this.filters.value.itemsPerPage}
                onChange={evt => {
                  this.search({ itemsPerPage: +evt.target.value });
                }}>
                {[2, 5, 10].map((opt, idx) => {
                  return <option key={idx}>{opt}</option>;
                })}
              </select>
            </div>
            <div className="col-9">
              <Pagination
                activePage={this.filters.value.currentPage}
                itemsCountPerPage={this.filters.value.itemsPerPage}
                totalItemsCount={this.props.total || 1}
                itemClass={"page-item"}
                linkClass={"page-link"}
                activeLinkClass={"active"}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </div>
            <div className="col-1">
              <div className="badge badge-info">
                items: {this.props.total}
              </div>
            </div>
          </div>
          <DataGridComponent
            data={this.props.data}
            config={this.props.dgConfig}
            buy={this.props.buy}
            more={this.more}>
            <button className="btn btn-primary" data-action='more'>more</button>
            <button className="btn btn-success" data-action='buy'>buy</button>
          </DataGridComponent>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { ...state.items, cart: state.cart };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetch() {
      dispatch(actions.fetchItems(this.filters.value));
    },
    more(id) {
      this.props.history.push("/items/" + id);
    },
    buy(id) {
      dispatch(actions.addToCard(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsContainer);
