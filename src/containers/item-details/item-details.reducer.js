import { GET_ITEM_SUCCESS } from "../actions";

export function itemDetailReducer(state = { data: false }, action) {
  switch (action.type) {
    case GET_ITEM_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}
