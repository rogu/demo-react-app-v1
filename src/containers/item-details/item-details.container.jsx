import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./item-details.actions";

class ItemDetailsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.get(this.props.match.params.id);
  }

  render() {
    return (
      <div className="jumbotron">
        <h2>name: {this.props.data.title}</h2>
        <img src={this.props.data.imgSrc} alt="img" width="150" />
        <p>price: {this.props.data.price}</p>
        <button onClick={this.props.history.goBack} className="btn btn-primary">
          back
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state.itemDetail });
const mapDispatchToProps = dispatch => {
  return {
    get(id) {
      dispatch(actions.getItem(id));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemDetailsContainer);
