import { Api } from "../../utils/api";
import { GET_ITEM_SUCCESS, Action } from "../actions";
import axios from "axios";

export function getItem(id) {
    return dispatch => {
        axios.get(`${Api.ITEMS_END_POINT}/${id}`).then(resp => {
            dispatch(new Action(GET_ITEM_SUCCESS, resp.data.data));
        });
    };
}
