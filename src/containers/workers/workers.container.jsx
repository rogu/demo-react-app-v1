import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './workers.actions';
import { DataGridComponent, SearchComponent, ModalComponent } from "../../components";
import { delay, getFormErrors } from "../../utils/helpers";
import AddWorkerComponent from './add-worker.component';

class WorkersContainer extends Component {

    constructor(props) {
        super(props);
        this.modal = React.createRef();
        this.addWorker = this.props.addWorker.bind(this);
        this.state = {
            formStatus: null
        }
    }

    componentDidMount() {
        this.props.fetch();
    }

    showId(id) {
        alert('selected id: ' + id)
    }

    remove = (id) => {
        if (window.confirm("are you sure?")) {
            this.props.remove(id);
        }
    }

    render() {
        return (
            <div className={'row'}>
                <div className="col-3">
                    <SearchComponent onChange={this.props.search} controls={['name', 'phone']} />
                    <hr />
                    <ModalComponent ref={this.modal} active={this.props.logged} text={"add worker"}>
                        <pre className="bg-warning">form status: {JSON.stringify(this.state.formStatus, null, 1)}</pre>
                        <AddWorkerComponent addWorker={this.addWorker}></AddWorkerComponent>
                    </ModalComponent>
                </div>
                <div className="col-9">
                    <DataGridComponent data={this.props.data}
                        config={this.props.dgConfig}
                        remove={this.remove}
                        more={this.showId}
                        update={this.props.update}
                        logged={this.props.logged} >
                        <button className="btn btn-danger" disabled={!this.props.logged} data-action='remove'>remove</button>
                        <button className="btn btn-info" data-action='more'>more</button>
                    </DataGridComponent>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { ...state.workers, logged: state.auth.logged };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetch() {
            dispatch(actions.fetchWorkers());
        },
        addWorker(evt) {
            evt.preventDefault();
            if (evt.target.checkValidity()) {
                const { name: { value: name }, phone: { value: phone }, category: { value: category } } = evt.target;
                dispatch(actions.addWorker({ name, phone, category }));
                this.modal.current.closeModal();
            } else {
                this.setState({ formStatus: getFormErrors(evt.target) });
            }
        },
        search(filter) {
            delay(() => dispatch(actions.searchWorkers(filter)));
        },
        remove(id) {
            dispatch(actions.removeWorker(id));
        },
        update({ target: { value, dataset: { id } } }) {
            delay(() => dispatch(actions.updateWorker(id, value)));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(WorkersContainer);
