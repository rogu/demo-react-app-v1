import React from 'react';

const AddWorkerComponent = (props) => {
    return (<form noValidate onSubmit={props.addWorker}>
        <div className="form-group"><input required type="text" className="form-control" placeholder="name" name="name" /></div>
        <div className="form-group"><input required type="number" className="form-control" placeholder="phone" name="phone" /></div>
        <div className="form-group">
            <select className="form-control" defaultValue="" required name="category">
                <option value="" disabled>category</option>
                <option value="support">support</option>
                <option value="sales">sales</option>
            </select>
        </div>
        <div className="form-group">
            <button className="btn btn-primary">save</button>
        </div>
    </form>);
}

export default AddWorkerComponent;
