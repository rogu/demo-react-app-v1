import { FETCH_WORKERS_SUCCESS, SEARCH_WORKERS } from "../actions";
import { FieldTypes } from "../../components/data-grid/data-grid.component";

const initialState = {
    data: [],
    dgConfig: [
        { key: 'name' },
        { key: 'phone', type: FieldTypes.input }
    ]
};

let dataCopy;

export function workersReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_WORKERS_SUCCESS:
            dataCopy = action.payload;
            return { ...state, data: action.payload };
        case SEARCH_WORKERS:
            return {
                ...state,
                data: dataCopy.filter(item => {
                    for (const key in action.payload) {
                        const prop = item[key].toString().toLowerCase();
                        const wanted = action.payload[key].toString().toLowerCase();
                        if (!prop.includes(wanted)) return false;
                    }
                    return true;
                })
            };
        default:
            return state;
    }
}
