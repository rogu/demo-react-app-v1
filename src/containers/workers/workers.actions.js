import axios from "axios";
import { Api } from "../../utils/api";
import { FETCH_WORKERS_SUCCESS, SEARCH_WORKERS, Action } from "../actions";

export function fetchWorkers() {
    return dispatch => {
        axios.get(Api.WORKERS_END_POINT)
            .then((resp) => {
                dispatch(new Action(FETCH_WORKERS_SUCCESS, resp.data.data));
            })
    }
}

export function searchWorkers(filters) {
    return { type: SEARCH_WORKERS, payload: filters };
}

export function addWorker(worker, filters) {
    return dispatch => {
        axios.post(Api.WORKERS_END_POINT, worker)
            .then((resp) => {
                dispatch(fetchWorkers(filters));
            })
    }
}

export function removeWorker(id) {
    return dispatch => {
        axios.delete(`${Api.WORKERS_END_POINT}/${id}`)
            .then((resp) => {
                dispatch(fetchWorkers());
            })
    }
}

export function updateWorker(id, phone) {
    return dispatch => {
        axios.patch(`${Api.WORKERS_END_POINT}/${id}`, { phone })
            .then((resp) => {
                dispatch(fetchWorkers());
            })
    }
}
