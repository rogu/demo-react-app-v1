import React, { Component } from 'react';
import { getFormErrors } from '../../utils/helpers';

export default class RegisterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formValue: {
                username: '',
                password: ''
            },
            formStatus: null
        }
    }
    /*
        toggleError({ validity, validationMessage: msg, nextElementSibling: nextEl }) {
            nextEl.classList[validity.valid ? 'add' : 'remove']('d-none');
            nextEl.innerText = msg;
        } */

    updateForm = ({ target }) => {
        //this.toggleError(target);
        this.setState({ formValue: { ...this.state.formValue, [target.name]: target.value } });
    }

    checkForm = (evt) => {
        evt.preventDefault();
        const formStatus = evt.target.checkValidity()
            ? true
            : getFormErrors(evt.target);
        this.setState({ formStatus });
    }

    render() {
        return <div className="row">
            <div className="col-6">
                <form className={'jumbotron'} noValidate onSubmit={this.checkForm}>
                    {Object.keys(this.state.formValue).map((item, idx) => {
                        return <div className="form-group" key={idx}>
                            <label>
                                {item}
                                <input type="text"
                                    required
                                    pattern="^[\w\s]{3,}"
                                    onChange={this.updateForm}
                                    placeholder="minlength 3"
                                    className="form-control"
                                    name={item} />
                            </label>
                        </div>
                    })}
                    <button className="btn btn-primary">send</button>
                </form>
            </div>
            <div className="col-6">
                <pre className="bg-warning">status: {JSON.stringify(this.state.formStatus, null, 1)}</pre>
                <hr />
                <pre>{JSON.stringify(this.state.formValue, null, 1)}</pre>
            </div>
        </div>
    }
}
