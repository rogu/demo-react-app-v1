
import Axios from "axios/index";

export function setCORSInterceptors() {
    Axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');
    Axios.interceptors.request.use((config) => {
        return { ...config, withCredentials: true };
    }, function (error) {
        return Promise.reject(error);
    });

    Axios.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        console.warn(`SERVER ERROR\n${JSON.stringify(error.response, null, 4)}`);
        return Promise.reject(error);
    });
}
