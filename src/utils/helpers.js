export const delay = (() => {
    let timer;
    return (cb, time = 500) => {
        clearTimeout(timer);
        timer = setTimeout(() => cb(), time);
    }
})();

export const getFormErrors = (form) => {
    return Array.from(form).reduce((acc, { name, tagName, validity, validationMessage: msg }) => {
        if (!/BUTTON/.test(tagName))
            return { ...acc, ...(!validity.valid && { [name]: msg }) };
        return acc;
    }, {});
}
